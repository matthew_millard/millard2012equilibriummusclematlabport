function dState = ...
    calcPrescribedMusculotendonStateDerivativeWrapper(t,...
                                                      state,...                                                      
                                                      prescribedPathFcn,...
                                                      prescribedActivationFcn,...
                                                      calcMuscleInfoFcn)
%%
% This is a wrapper ultimately to create a derivative function that takes t
% and muscle state as arguments and returns the muscle state derivative,
% and the various powers of interest, as the muscle undergoes a constant
% activation sinusoidal stretch benchmark simulation
%
% @param t: time in seconds
%
% @param state: the state vector for this benchmark simulation. This vector
%               contains:
%
%               [muscleState;
%                boundaryPower;
%                activeFiberPower;
%                dampingPower]
% 
%                The last three entries are required to numerically
%                evaluate T + V - W = const to ensure that the model is
%                conservative.
%
% @param prescribedPathFcn : a handle to a function that given time t
%                            produces a 2x1 vector containing the velocity 
%                            and length of the path the muscle lies on.
%
% @param prescribedActivationFcn: a handle to a function that given time t
%                            produces a 1x1 scalar of the activation of the
%                            muscle.
%
% @param calcMuscleInfoFcn: a function handle to a muscle function
%                      like (calcMillard2012DampedEquilibriumMuscleInfo.m) 
%                      and takes arguments of activation, pathState, and 
%                      muscle state
%
% @returns dState: the first time derivative of the state vector
%
%               [muscleState;
%                boundaryPower;
%                activeFiberPower;
%                dampingPower]
%
%
%%
                                                  
pathState       = prescribedPathFcn(t);
activationState = prescribedActivationFcn(t);

mtInfo = calcMuscleInfoFcn(activationState,...
                           pathState,...
                           state);      

dlp = pathState(1);                       

boundaryPower       = mtInfo.muscleDynamicsInfo.boundaryPower;
activeFiberPower    = mtInfo.muscleDynamicsInfo.fiberActivePower;
dampingPower        = mtInfo.muscleDynamicsInfo.dampingPower;


dState = [];

if(length(state) > 3)
    dState = [mtInfo.state.derivative; ...
               boundaryPower;...
               activeFiberPower;...
               dampingPower];    
else
    dState = [ boundaryPower;...
               activeFiberPower;...
               dampingPower]; 
end

