function idx = getArnold2010MuscleIndex(abbrev, abbreviationList)

idx = NaN;
i = 1;
while isnan(idx)==1 && i <= length(abbreviationList)
   if(strcmp(abbrev,abbreviationList{i}))
       idx = i;
   end
   i=i+1;
end
