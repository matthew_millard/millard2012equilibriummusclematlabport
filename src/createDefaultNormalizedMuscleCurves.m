function normMuscleCurves = createDefaultNormalizedMuscleCurves(muscleName, flag_plotCurves)
%%
% This function constructs the default normalized muscle fiber and tendon
% characteristic curves that are used in the Millard2012EquilibriumMuscle
% model in OpenSim and puts them into a struct.
%
% @param muscleName: the name of the muscle. This string is used to give
%                    each of the curves a specific name
%
% @param flag_plotCurves: 0: nothing will happen
%                         1: each curves value, 1st, and 2nd derivative
%                         will be plotted over the full Beziercurve domain 
%                         and a little beyond so that you can see
%                         extrapolation. If the curve has an integral then
%                         its value too will be plotted
%                         
% @return normMuscleCurves: a struct containing the following fields
%
%          .activeForceLengthCurve
%          .fiberForceLengthCurve
%          .fiberForceVelocityCurve
%          .tendonForceLengthCurve
%
%          .activeForceLengthCurveHACK
%          .fiberForceVelocityInverseCurveHACK
%                       
%
%%
normMuscleCurves = [];

if(exist('normMuscleCurves.mat','file') == 2)
    tmp = load('normMuscleCurves.mat');
    normMuscleCurves = tmp.normMuscleCurves;
else    
    %%
    %Active force length curve
    %%
    lce0 = 0.47-0.0259;
    lce1 = 0.73;
    lce2 = 1.0;
    lce3 = 1.8123;
    minActiveForceLengthValue = 0;
    curviness = 1.0;
    computeIntegral = 0;
    plateauSlope = 0.8616;

    activeForceLengthCurve = createFiberActiveForceLengthCurve( lce0,...
                                                                lce1, ...
                                                                lce2, ...
                                                                lce3, ...
                                                                minActiveForceLengthValue,...
                                                                plateauSlope, ...
                                                                curviness, ...
                                                                computeIntegral, ...
                                                                muscleName);  

    normMuscleCurves.activeForceLengthCurve = activeForceLengthCurve;

    %%
    %Fiber Passive Force
    %%
    eZero = 0; 
    eIso  = 0.7;
    kLow  = 0.2;
    kIso  = 2/(eIso-eZero);
    curviness = 0.75;
    computeIntegral = 1;

    fiberForceLengthCurve = createFiberForceLengthCurve(eZero,...
                                                        eIso,...
                                                        kLow,...
                                                        kIso,...
                                                        curviness,...
                                                        computeIntegral,...
                                                        muscleName);        
    normMuscleCurves.fiberForceLengthCurve = fiberForceLengthCurve;

    %%
    %Fiber Force Velocity Curve
    %%                                                
    fmaxE         = 1.4;
    dydxC         = 0;
    dydxNearC     = 0.15;
    dydxIso       = 5.0;
    dydxE         = 0;
    dydxNearE     = 0.15;
    concCurviness = 0.6;
    eccCurviness  = 0.9;
    computeIntegral = 0;

    fiberForceVelocityCurve =  createFiberForceVelocityCurve(...
                                            fmaxE, ...
                                            dydxC,dydxNearC, ...
                                            dydxIso, ...
                                            dydxE, dydxNearE,...
                                            concCurviness, eccCurviness,...
                                            computeIntegral, muscleName);

    normMuscleCurves.fiberForceVelocityCurve = fiberForceVelocityCurve;
    %%
    %Tendon Force Length Curve
    %%
    eIso            = 0.049;
    kIso            = 1.375/eIso;
    fToe            = 2.0/3.0;
    curviness       = 0.5;
    computeIntegral = 1;

    tendonForceLengthCurve = ...
              createTendonForceLengthCurve( eIso, kIso, ...
                                            fToe, curviness, ...
                                            computeIntegral, ...
                                            muscleName);

    normMuscleCurves.tendonForceLengthCurve = tendonForceLengthCurve;

    %%
    % HACKED 'Classic' muscle modeling curves
    %
    % Almost all classic elastic tendon Hill-type muscle models take the
    % tendon-fiber equilibrium force equation:  
    %
    %    a(fl(lce)*fv(dlce/dt) + fpe(lce))*cos(alpha) - ft(lt) = 0;
    %
    % and massage it into an ode:
    %
    %    dlce/dt = fv^-1 ( [ (ft / a*cos(alpha)) - fpe] / fl)
    %
    % Which goes singular when ever:
    %
    %  a -> 0
    %  cos(alpha) -> 0
    %  fl -> 0
    %  fv^-1 -> inf as dlce/dt -> vmax
    %
    % To use this model without causing singularities means
    %
    %  a > 0      : the muscle cannot turn off          -> not physically true
    %  alpha < 90 : the pennation angle cannot go to 90 -> probably correct
    %  fl > 0     : the fiber can always generate force -> not physically true
    %  fv^-1      : the fiber can generate force at all
    %               velocities, and can even generate
    %               compressive forces at high shortening
    %               velocities                          -> not true.
    %%

    %%
    %*HACKED Fiber Active Force Length Curve
    %%

    lce0 = 0.47-0.0259;
    lce1 = 0.73;
    lce2 = 1.0;
    lce3 = 1.8123;
    minActiveForceLengthValue = 0.1; %*Here's the hack: minimum value > 0
    curviness = 1.0;                 % in many papers this value really 
    computeIntegral = 0;             % is 0.1! This is huge!
    plateauSlope = 0.8616;

    activeForceLengthCurveHack = createFiberActiveForceLengthCurve( lce0,...
                                                                lce1, ...
                                                                lce2, ...
                                                                lce3, ...
                                                                minActiveForceLengthValue,...
                                                                plateauSlope, ...
                                                                curviness, ...
                                                                computeIntegral, ...
                                                                muscleName);

    normMuscleCurves.activeForceLengthCurveHACK = activeForceLengthCurveHack;
    %%
    %*HACKED Fiber Force Velocity Inverse Curve
    %%

    fmaxE         = 1.4;
    dydxC         = 0.15; %Hack: These are not zero
    dydxNearC     = 0.15;
    dydxIso       = 5.0;
    dydxE         = 0.15; %Hack: These are not zero
    dydxNearE     = 0.15;
    concCurviness = 0.6;
    eccCurviness  = 0.9;
    computeIntegral = 0;


    fiberForceVelocityInverseCurveHack = ...    
        createFiberForceVelocityInverseCurve(fmaxE, ...
                                             dydxC, dydxNearC, ...
                                             dydxIso,...
                                             dydxE, dydxNearE,...
                                             concCurviness, eccCurviness,...
                                             computeIntegral, muscleName);

    normMuscleCurves.fiberForceVelocityInverseCurveHACK = fiberForceVelocityInverseCurveHack;
    

    fiberForceVelocityCurveHack = ...    
        createFiberForceVelocityCurve(fmaxE, ...
                                     dydxC, dydxNearC, ...
                                     dydxIso,...
                                     dydxE, dydxNearE,...
                                     concCurviness, eccCurviness,...
                                     computeIntegral, muscleName);

    normMuscleCurves.fiberForceVelocityCurveHACK = fiberForceVelocityCurveHack;
    
    %%
    %Generate plots of the curves using the function 
    % calcNormalizedMuscleCurveDerivative
    %%

    save('normMuscleCurves.mat','normMuscleCurves');
end

if(flag_plotCurves ==1)
    
    curveParamVector(1).struct = normMuscleCurves.activeForceLengthCurve;
    curveParamVector(2).struct = normMuscleCurves.fiberForceLengthCurve;
    curveParamVector(3).struct = normMuscleCurves.fiberForceVelocityCurve;
    curveParamVector(4).struct = normMuscleCurves.tendonForceLengthCurve;

    curveParamVector(5).struct = normMuscleCurves.activeForceLengthCurveHACK;
    curveParamVector(6).struct = normMuscleCurves.fiberForceVelocityInverseCurveHACK;
    curveParamVector(7).struct = normMuscleCurves.fiberForceVelocityCurveHACK;
    
    for i=1:1:length(curveParamVector)
        fig(i) = figure;

        curveSample = calcBezierYFcnXCurveSampleVector(...
                                        curveParamVector(i).struct, 100);


        xmin = min(curveSample.x);
        xmax = max(curveSample.x);

        ymin = min(curveSample.y);
        ymax = max(curveSample.y);
        yDelta = ymax-ymin;


        xV   = curveSample.x;
        yV   = curveSample.y;
        y1V  = curveSample.dydx;
        y2V  = curveSample.d2ydx2;

        xStr = 'Norm. Fiber Length (lceN)';
        yStr = 'Norm. Active Force (flN)';
        titleStr = curveParamVector(i).struct.name;


        subplot(2,2,1);
            plot(xV,yV,'b');
            xlabel(xStr);
            ylabel(yStr);
            title(titleStr);
            grid on;
            hold on;
                xlim([xmin,xmax]);
                ylim([ymin-0.1*yDelta,ymax+0.1*yDelta]);

        subplot(2,2,2);        
            plot(xV,y1V,'r');
            xlabel(xStr);
            ylabel(['d/dlceN ',yStr]);
            title(['d/dlceN ',titleStr]);

            grid on;
            hold on;

                xlim([xmin,xmax]);                                

        subplot(2,2,3);        
            plot(xV,y2V,'m');
            xlabel(xStr);
            ylabel(['d^2/dlceN^2 ',yStr]);
            title(['d^2/dlceN^2 ',titleStr]);
            grid on;
            hold on;
            xlim([xmin,xmax]);       

        if(isempty(curveParamVector(i).struct.integral)==0)  
            intYdx = curveSample.intYdx;

            subplot(2,2,4)
                plot(xV, intYdx,'g');
                xlabel(xStr);
                ylabel(['intYdx ',yStr]);
                title(['intYdx ',titleStr]);
                grid on;
                hold on;
                xlim([xmin,xmax]);  

        end

    end
end
    





            