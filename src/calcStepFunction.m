function y = calcStepFunction(t, ton, toff)

y = 0;
if(t >= ton && t <= toff)
    y = 1;
end