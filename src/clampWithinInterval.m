function uC = clampWithinInterval(u,umin,umax)
%%
% This function clamps u to be within the interval [0,1]
%
% @param u
% @return uC, version of u that is guaranteed to be within [0,1]
%%
uC = u;
if(u<umin)
    uC=umin;
end
if(u>umax)
    uC=umax;
end

