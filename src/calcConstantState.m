function state = calcConstantState(t,c)
%%
% A function that returns a constant value for all time. This exists merely
% to provide a standard function interface.
%
% @param t: time
% @param c: the constant value to return.
% @return state which has the value c
%%
state = c;