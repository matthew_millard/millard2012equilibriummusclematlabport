function sinState = calcSinusoidState(t,a,b,omega)
%%
% Calculates the value of a sinusoid function at time t. The function is
% parameterized by a constant offset, magnitude and angular velocity omega:
%
%   y(t) = a + b*sin(omega*t)
%
% @param t: time
% @param a: offset
% @param b: amplitude of the sine function
% @param omega: frequency of the sine funtion in radians/sec
%
% @returns sinState 2x1 vector:
%          sinState(1) = dy(t)/dt 
%          sinState(2) =  y(t)
%
%%

sinState = zeros(2,1);

sinState(1) = b*cos(omega*t)*omega;
sinState(2) = a + b*sin(omega*t);
