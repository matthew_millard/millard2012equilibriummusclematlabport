function Dalpha_Dlce = ...
    calcFixedWidthPennationDalphaDlce( alpha,...
                                       fiberLength,...
                                       optimalFiberLength,...
                                       pennationAngleAtOptimalFiberLength)
%%
%This function evaluates the partial derivative of the pennation angle 
%w.r.t. a change in fiber length.
%
% @param alpha: pennation angle of the fiber (radians)
% @param fiberLength (m)
% @param optimalFiberLength (m)
% @param pennationAngleAtOptimalFiberLength (radians)
%
% @return Dalpha_Dlce the partial derivative of the pennation angle w.r.t.
%                     the fiber length
%%
                                                   
Dalpha_Dlce = 0;

if(pennationAngleAtOptimalFiberLength > eps^0.5)
    lce       = fiberLength;   
    lceOpt    = optimalFiberLength;         
    alphaOpt  = pennationAngleAtOptimalFiberLength;     

    lceAT     = lce*cos(alpha);    
    assert(lceAT > eps^0.5,...
           ['Impending singularity: lceAT < eps^0.5 ']);
        
    h         = lceOpt*sin(alphaOpt);

    x         = h/lceAT;
    dxdlce    = -h*cos(alpha)/(lceAT*lceAT);

    Dalpha_Dlce = (1/(1 + x*x))*dxdlce;
end

                                                     