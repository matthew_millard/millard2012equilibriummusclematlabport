function idx = calcIndex(x, ptsM,options)
%%
% Given a series of sequential ranges in the 2xN matrix
% ptsM, this function will calculate the row index that
% has an interval ptsM(n,1)-ptsM(n,2) that contains the
% point x. If x lies on a border that defines a sub-interval
% the function will return the index such that x is at the 
% start of hte interval.
%
% @param x: a double 
% @param ptsM: a 2 x n matrix that defines the range of n
%              sub intervals. For example
%
%
%        ptsM = [0 1 2 3 4; ...
%                1 2 3 4 5]
%
%        defines 5 adjacent sub intervals 
%        [[0,1],[1,2],[2,3],[3,4],[4,5]]
%
% @param tol: how close a value x is allowed to be to a
%             sub interval border before it is declared to be
%             on the border.
%
% @returns idx: the column index of ptsM that contains an
%               interval that includes x.
%
% Example:
% 1. x = .123
%    ptsM = [0 1 2 3 4; ...
%            1 2 3 4 5]
%  then calcIndex will return 1
%
% 2. x = 1
%    ptsM as before
%    then calcIndex will return 2.
%
%%
x0 = x;
idx = 0;
flag_found = 0;

rows = size(ptsM,1);

tol = options.tol;

if( isempty( options.moduloRange ) == 0 && ...
           (x > options.moduloRange || x < 0 ) )
    if(x < 0 || x > options.moduloRange)        
        x = mod(x,options.moduloRange);
    end 
    if((x >= 0 && x <= options.moduloRange) == 0)
       fprintf('x: %e xmin: %e xmax: %e\n',...
                x, 0, options.moduloRange); 
    end
    assert( (x >= 0 && x <= options.moduloRange),'x is out of range');
end

for i=1:1:size(ptsM,2)
    if( (x >= ptsM(1,i) && x < ptsM(rows,i)) || ... 
        (abs(x-ptsM(1,i)) < tol || abs(x-ptsM(rows,i)) < tol) )
        idx = i;
        flag_found = 1;
    end
end

%Check if the value x is identically the last point
if(flag_found == 0 && x == ptsM(rows,size(ptsM,2)))
    idx = size(ptsM,2);
    flag_found = 1;
end

if(flag_found==0)
   here=1; 
end

assert( (flag_found == 1),... 
    'Error: A value of x was used that is not within the Bezier curve set.');
