function val = calcCentralDifference(x, fcn)
%%
% Calculates a central difference of the function fcn about x
%
% @param x    : the argument to the function fcn
% @param fcn  : a function handle to fcn(x)
% @returns val: the value of the central difference of fcn at x
%%

val = NaN;
h = eps^0.5;

val = (fcn(x+h)-fcn(x-h))/(2*h);