author: M.Millard
date  : March 2015 - first written
      : May 2018   - cleaned up
--------------------------------------------------------------------------------
This repository contains a Matlab implementation of the muscle model described
in this paper:

Millard, M., Uchida, T., Seth, A., & Delp, S. L. (2013). Flexing computational 
muscle: modeling and simulation of musculotendon dynamics. Journal of 
biomechanical engineering, 135(2), 021005.

--------------------------------------------------------------------------------
Things to keep in mind:
--------------------------------------------------------------------------------

  This is an implementation that has been made for teaching, thus it is been
  coded so that it is relatively easy to read, but is slow. If you need 
  something faster please use the C++ implementation provided by OpenSim: it is
  around 100x faster than this Matlab implementation. Furthermore, you can 
  access this implementation using the python/matlab interfaces that OpenSim
  provides: not knowing C++ is not an excuse that should stop you from using
  the official implementation. 

  That said, if you want to dig into the details of how this model
  works, you might find this Matlab implementation easier to explore.
  This implementation may differ in subtle ways with the OpenSim implemenation:
  there has been no effort made to keep this implementation syncronized with
  OpenSim's C++ implementation.

  Finally, this script can be made to run in octave. To do so, simply change
  the call to ode45 on line 224 of runMillard2012ComputationalBenchmark.m 
  to use octaves lsode integrator.

--------------------------------------------------------------------------------
Getting started:
--------------------------------------------------------------------------------

1. Run main.m from Matlab: this should produce plots that show all of 
   the normalized muscle curve values, and first 2 derivatives.

   Now in main.m set

     flag_plotNormMuscleCurves = 0;

   unless you want these plots popping up all the time.

2. Run a sinusoidal-stretch constant activation simulation of a muscle: set
   one or more of these flags to 1 in main.m. For example:

   flag_runRigidBench               = 0;
   flag_runClassicElasticBench      = 0;
   flag_runDampedFiberElasticBench  = 1;

   *These simulations will take a few minutes, so be patient. Perhaps read the
   rest of the text below while its running.

--------------------------------------------------------------------------------
Variables to play with in main.m, near the top
--------------------------------------------------------------------------------

  1. flag_useArnold2010SoleusArchitecture = 0;
     muscleAbbrArnold2010                 = 'soleus';

      :Setting the flag to 1 will mean the muscle architecture will match the 
       soleus muscle that appears in Arnold et al. You can also choose from a 
       variety of other architectures using an abbreviation from the 
       file 'arnold2010LegMuscleArchitectureAbbreviation.txt'.

    Arnold, E. M., Ward, S. R., Lieber, R. L., & Delp, S. L. (2010). A model of 
    the lower limb for analysis of human movement. Annals of biomedical 
    engineering, 38(2), 269-279.

  2. normPathStretch = 1; 
     cycleTime       = 1; 

     Changes the length of the sinusoidal stretch and the period of the sin
     wave.

--------------------------------------------------------------------------------
Files to have a look at for further details:
--------------------------------------------------------------------------------

  1. calcMillard2012DampedEquilibriumMuscleInfo.m
    Evaluates the state derivative and a large number of variables of interest.

  2. calcInitialMuscleState.m
    Initializes the muscle state.

  3. Code that creates the Bezier curves for each muscle:

      createCurveIntegralStructure.m
      createDefaultNormalizedMuscleCurves.m
      createFiberActiveForceLengthCurve.m
      createFiberForceLengthCurve.m
      createFiberForceVelocityCurve.m
      createFiberForceVelocityInverseCurve.m
      createTendonForceLengthCurve.m

  4. Code that evaluates the Bezier curves

    calc1DBezierCurveValue.m
    calc5thOrderInterp.m
    calcBezierYFcnXCurveSampleVector.m
    calcBezierYFcnXDerivative.m

  5. Code that evaluates the pennation model

    calcFixedWidthPennatedFiberKinematicsAlongTendon.m
    calcFixedWidthPennatedFiberKinematics.m
    calcFixedWidthPennatedFiberMinimumLength.m
    calcFixedWidthPennationDalphaDlce.m

  6. Code that evaluates the state derivative of activation

    calcFirstOrderActivationDerivative.m

  7. runMillard2012ComputationalBenchmark.m
    The script that runs the sinusoidal-stretch constant activation simulations.


Have fun - happy exploring!